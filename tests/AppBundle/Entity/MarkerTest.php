<?php
namespace Tests\AppBundle\Entity;

use AppBundle\Entity\Marker;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validation;


class MarkerTest extends KernelTestCase
{
  private $em;
  private $validator;

  /**
   * {@inheritDoc}
   */
  protected function setUp()
  {
      self::bootKernel();

      $this->em = static::$kernel->getContainer()
          ->get('doctrine')
          ->getManager();

      $this->validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
  }

  public function testIsValid()
  {
      $entity = new Marker();

      $entity->setLat(10);
      $entity->setLat(10);
      $entity->setColor(Marker::$COLORS[0]);

      $errors = $this->validator->validate($entity);
      $this->assertEquals(0, count($errors));

  }

  public function testNotValid()
  {
    $entity = new Marker();

    $entity->setLat(-200);
    $entity->setLat(-200);

    $errors = $this->validator->validate($entity);
    $this->assertGreaterThan(0, count($errors));
  }

  /**
   * {@inheritDoc}
   */
  protected function tearDown()
  {
      parent::tearDown();
      $this->em->close();
  }
}
