var selectedMarker = null;
var openedInfoWindow = null;
var map = null;

var gMap = {
    /**
     * Inicjowanie mapy google
     */
    initMap: function () {
        var self = this;
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 6,
            center: { lat: 52, lng: 19 },
        });

        map.addListener('click', function (e) {
            self.addMarker({position: e.latLng}, true, false);
        });

        this.placesAutocomplete();
        this.loadAllMarkers();
    },

    /**
     * Inicjowanie autocomplete
     */
    placesAutocomplete: function () {
        var self = this;
        var input = $('#place').get(0);
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed', function () {
            var place = autocomplete.getPlace();
            if (!place.geometry)
                return;

            map.setCenter(place.geometry.location);
            self.addMarker({position: place.geometry.location}, true, false);
        });
    },

    /**
     * Otwiera infowindow przy znaczniku i ustawia dane w formularzu
     * dodatkowo ustawia aktualnie wybrany znacznik - selectedMarker
     * @param marker - instancja znacznika mapy google
     */
    openInfoWindow: function (marker) {
        selectedMarker = marker;
        infowindow = new google.maps.InfoWindow({
            content: $('#info-window-wrapper').html()
        });

        if (openedInfoWindow)
            openedInfoWindow.close();

        google.maps.event.addListener(infowindow, 'domready', function(){
            var markerForm = $('form[name="marker-form"]');
            markerForm.find('[name="description"]').val(marker.description);
            markerForm.find('input[value="'+marker.color+'"]').prop("checked", true);
        });

        infowindow.open(map, marker);
        openedInfoWindow = infowindow;
    },

    /**
     * Zamyka infowindow
     * dodatkowo ustawa aktualnie wybrany znacznik z mapy na null - selectedMarker
     */
    closeInfoWindow: function () {
        if (openedInfoWindow)
            openedInfoWindow.close();
        openedInfoWindow = null;
        selectedMarker = null;
    },

    /**
     * Dodawanie znacznika do listy
     * @param marker - instancja znacznika mapy google
     */
    addToList: function(marker) {
      var list = $('#markers-list');
      var liElement = list.find('[data-marker-id="'+marker.id+'"]');
      var newLiElement = [
        '<li onclick="gMap.goToMarker(this)" class="list-group-item marker-item" ',
        'data-marker-lat="'+marker.position.lat()+'" data-marker-lng="'+marker.position.lng()+'"',
        'data-marker-id="'+marker.id+'">',
        '<b>'+marker.description+'</b> ',
        '('+marker.position.lat() +', '+ marker.position.lng()+')',
        '<span style="color:'+marker.color+';" class="glyphicon glyphicon-map-marker"></span>',
        '</li>'
      ];

      if( liElement.length === 0 ){
        list.append(newLiElement.join(''));
      } else {
        liElement.replaceWith(newLiElement.join(''));
      }
    },

    /**
     * Usuwanie znacznika z listy
     * @param id - id znacznika
     */
    removeFromList: function(id) {
      var list = $('#markers-list');
      list.find('[data-marker-id="'+id+'"]').remove();
    },

    /**
     * Centrowanie mapy na wybranym znaczniku z listy
     * @param e - kliknięty element
     */
    goToMarker: function(e){
        var location = new google.maps.LatLng( $(e).data('markerLat'), $(e).data('markerLng'));
        map.setCenter(location);
    },

    /**
     * Dodaje znacznik na mapie, umożliwia jego jednoczesny zapis i otwarcie infowindow
     * @param marker - obiekt który zostaje umieszczony na mapie w postaci znacznika
     * @param openInfowindow - czy po dodaniu otworzyć infowindow
     * @param onlyDisplay - czy znacznik ma być tylko wyświetlony (true) czy wyświetlony i zapisany (false)
     */
    addMarker: function (marker, openInfowindow, onlyDisplay) {
        var self = this;
        var list = $('#markers-list');
        marker.map = map;
        marker = new google.maps.Marker(marker);
        marker.addListener('click', function () {
            self.openInfoWindow(marker);
        });

        if (openInfowindow) {
            self.openInfoWindow(marker);
        } else {
            self.addToList(marker);
        }

        if(!onlyDisplay) {
            self.saveMarker(openInfowindow);
        }

        marker.setIcon(self.gerMarkerIcon(marker.color));

        google.maps.event.addListener(marker, 'mouseover', function() {
            list.find('[data-marker-id="'+marker.id+'"]').addClass('active');
        });
        google.maps.event.addListener(marker, 'mouseout', function() {
            list.find('[data-marker-id="'+marker.id+'"]').removeClass('active');
        });
    },

    /**
     * Dociąga wszystkie znaczniki z bazy i umieszcza je na mapie
     */
    loadAllMarkers: function () {
        var self = this;
        $.ajax({
            type: 'GET',
            url: '/marker/getAll',
            success: (function (response) {
                for (var i in response) {
                    response[i].position = new google.maps.LatLng(response[i].lat, response[i].lng);
                    self.addMarker(response[i], false, true);
                }
            })
        });
    },

    /**
     * Zwraca adres ikony markera dla wybranego koloru
     * @param color
     * @returns {string}
     */
    gerMarkerIcon: function(color) {
      return 'http://maps.google.com/mapfiles/ms/icons/'+color+'-dot.png';
    },

    /**
     * Zwraca tylko niezbędne do zapisania znacznika dane
     * dla nowych znaczników id ustawiane na -1
     * @returns {{id: (number, description: *, color: string, lat: float, lng: float}}
     */
    getMarkerData: function () {
        var markerForm = $('form[name="marker-form"]');

        selectedMarker.description =  markerForm.find('[name="description"]').val();
        selectedMarker.color = markerForm.find('input[name="color"]:checked').val();
        var data = {
            id: selectedMarker.id,
            description: selectedMarker.description,
            color: selectedMarker.color,
            lat: selectedMarker.position.lat(),
            lng: selectedMarker.position.lng()
        };

        if(typeof data.id === 'undefined')
            data.id = -1;

        return data;
    },

    /**
     * Funkcja do zapisu markera w bazie
     * @param keepInfowindow - czy po zapisaniu automatycznie zamknąć infowindow
     */
    saveMarker: function (keepInfowindow) {
        var self = this;
        var marker = this.getMarkerData();
        var url = '/marker/save/' + (marker.id > -1 ? marker.id : '');
        $.ajax({
            type: 'POST',
            url: url,
            data: marker,
            success: (function (response) {
                selectedMarker.id = response.id;
                selectedMarker.setIcon(self.gerMarkerIcon(selectedMarker.color));
                self.addToList(selectedMarker);

                if(!keepInfowindow)
                    self.closeInfoWindow();
            })
        });
    },

    /**
     * Funkcja do usuwania z znacznika z bazy i mapy
     * id jest brane z aktualnie otwartego markera - selectedMarker
     */
    removeMarker: function () {
        var self = this;
        $.ajax({
            type: 'GET',
            url: '/marker/remove/'+selectedMarker.id,
            success: (function (response) {
                self.removeFromList(selectedMarker.id);
                selectedMarker.setMap(null);
                self.closeInfoWindow();
            })
        });
    },

};
