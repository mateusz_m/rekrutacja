<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity;
use AppBundle\Entity\Marker;
use Doctrine\ORM\Query;

class DefaultController extends Controller
{


    /**
     * Główna akcja do renderowania strony
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render(
            'default/index.html.twig',[
            'colors' => Marker::$COLORS
        ]);
    }

    /**
     * Akcja do pobierania listy markerów
     * @Route("/marker/getAll")
     */
    public function getAllMarkersAction(Request $request)
    {
        $query = $this->getDoctrine()
            ->getRepository('AppBundle:Marker')
            ->createQueryBuilder('c')
            ->getQuery();
        $markers = $query->getResult(Query::HYDRATE_ARRAY);
        return new JsonResponse( $markers );
    }


    /**
     * Dodawanie nowego lub aktualizacja istniejącego markera
     * @Route("/marker/save/")
     * @Route("/marker/save/{id}", requirements={"id": "\d+"})
     */
    public function saveMarkerAction(Request $request, $id=-1)
    {
        $em = $this->getDoctrine()->getManager();
        $marker = $em->getRepository('AppBundle:Marker')->find($id);
        if (!$marker)
            $marker = new Marker();

        $marker->setDescription($request->get('description'));
        $marker->setColor($request->get('color'));
        $marker->setLat($request->get('lat'));
        $marker->setLng($request->get('lng'));

        $validator = $this->get('validator');
        $errors = $validator->validate($marker);

        if(count($errors)){
            return new Response('Error', 500);
        } else {
            if($id == -1)
                $em->persist($marker);
            $em->flush();
            return new JsonResponse([
                'id'=> $marker->getId()
            ]);
        }
    }

    /**
     * Usuwanie markera
     * @Route("/marker/remove/{id}", requirements={"id": "\d+"})
     */
    public function removeMarkerAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $marker = $em->getRepository('AppBundle:Marker')->find($request->get('id'));
        if ($marker){
            $em->remove($marker);
            $em->flush();
        }

        return new JsonResponse();
    }

}
